package com.example.demo;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;

@RestController
public class HelloController {

	private DogService service;
	
	@RequestMapping("/")
	public String index() {
		String dogNoise = service.getNoise();
		return "Hello World!  Dogs go '" + dogNoise + "'";
	}
	
	@Autowired
    public void setService(DogService service) {        
        this.service = service;
	}
	
}
