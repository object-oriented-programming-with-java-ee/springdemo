package com.example.demo;

import org.springframework.stereotype.Component;

@Component
public class DogService {

	public String getNoise() {
		return "Woof!";
	}
	
}
